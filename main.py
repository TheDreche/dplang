import sys

print("""Wilkommen zu dp der Deutschen Programmiersprache. Dateien als .dp
      abspeichern dann dieses Skript ausführen.""")
datei = input("Datei: ")

# Funktionen

def typee(a):
    if a.startswith('"'):
        return a.replace('"','')
    if a.startswith("'"):
        return a.replace("'","")
    else:
        print("Error")
        sys.exit(0)
# Auslesen
d = open(datei)

for z in d:
    if z.startswith("ausgeben"):
        merken = z.replace(")","").replace("ausgeben(", "")
        print(typee(merken))
